import numpy as np
import matplotlib.pyplot as plt

def read_data_from_file(filename, multiplier=0.000001):
    with open(filename, 'r') as f:
        lines = f.readlines()

    
    lines = [line for line in lines if line.strip() and not line.startswith("#")]

    
    steps = []
    values = []
    for line in lines:
        parts = line.split()
        steps.append(float(parts[0]) * multiplier)
        values.append(float(parts[1]))

    return steps, values

steps1, values1 = read_data_from_file('5ns_colvars.traj')
steps2, values2 = read_data_from_file('10ns_colvars.traj')
steps3, values3 = read_data_from_file('100ns_colvars.traj', multiplier=0.000002)


plt.figure(figsize=(10,6))
plt.plot(steps1, values1, '-o', label='5ns')
plt.plot(steps2, values2, '-o', label='10ns', linestyle='--')
plt.plot(steps3, values3, '-o', label='100ns', linestyle='-.')
plt.xlabel('Tiempo (ns)')
plt.ylabel('Coordenada de transición')
plt.title('Grafico de muestreo')
plt.legend()
plt.grid(True)
plt.show()

