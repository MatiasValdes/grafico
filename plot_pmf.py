import pandas as pd
import matplotlib.pyplot as plt

data_5ns = pd.read_csv('5ns.pmf', delim_whitespace=True, skiprows=1, header=None, names=['xi', 'A(xi)'])
data_10ns = pd.read_csv('10ns.pmf', delim_whitespace=True, skiprows=1, header=None, names=['xi', 'A(xi)'])
data_100ns = pd.read_csv('100ns.pmf', delim_whitespace=True, skiprows=1, header=None, names=['xi', 'A(xi)'])

plt.figure(figsize=(10,6))


plt.plot(data_5ns['xi'], data_5ns['A(xi)'], label='5ns', color='red')
plt.plot(data_10ns['xi'], data_10ns['A(xi)'], label='10ns', color='blue')
plt.plot(data_100ns['xi'], data_100ns['A(xi)'], label='100ns', color='green')


plt.xlabel('Distancia (A)')
plt.ylabel('Energia libre (kcal/mol)')
plt.title('Comparación PMF de 5ns,10ns y 100ns')
plt.legend()
plt.grid(True)


plt.show()

